<?php

declare(strict_types=1);

namespace Drupal\Tests\copyright_notice\Kernel;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Tests the entity API for the copyright_notice field type.
 *
 * @group copyright_notice
 */
class CopyrightNoticeItemTest extends FieldKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['copyright_notice'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a copyright_notice field storage and field for validation.
    FieldStorageConfig::create([
      'field_name' => 'field_test',
      'entity_type' => 'entity_test',
      'type' => 'copyright_notice',
    ])->save();
    FieldConfig::create([
      'entity_type' => 'entity_test',
      'field_name' => 'field_test',
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * Tests using entity fields of the copyright_notice field type.
   */
  public function testCopyrightNoticeItem(): void {
    $year = 1982;

    // Verify entity creation.
    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
      'field_test' => [
        'value' => $year,
      ],
    ]);
    $entity->save();

    // Verify entity has been created properly.
    $id = $entity->id();
    $entity = EntityTest::load($id);
    $this->assertInstanceOf(FieldItemListInterface::class, $entity->get('field_test'));
    $this->assertInstanceOf(FieldItemInterface::class, $entity->get('field_test')->first());
    $this->assertEquals($entity->get('field_test')->first()->get('value')->getValue(), $year);

    // Verify changing the field value.
    $new_year = 2001;
    $entity->get('field_test')->first()->set('value', $new_year);
    $this->assertEquals($entity->get('field_test')->first()->get('value')->getValue(), $new_year);

    // Read changed entity and assert changed values.
    $entity->save();
    $entity = EntityTest::load($id);
    $this->assertEquals($entity->get('field_test')->first()->get('value')->getValue(), $new_year);
  }

}
