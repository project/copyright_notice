<?php

declare(strict_types=1);

namespace Drupal\Tests\copyright_notice\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests rendering of the #copyright_notice theme hook.
 *
 * @group copyright_notice
 */
class RenderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['copyright_notice'];

  /**
   * Tests rendering of the #copyright_notice theme hook.
   *
   * @dataProvider copyRightNoticeProvider
   */
  public function testCopyrightNoticeRendering(array $element, string $expected): void {
    $this->assertThemeOutput('copyright_notice', $element, $expected);
  }

  /**
   * Provides data for testCopyrightNoticeRendering().
   *
   * @return array
   *   An array of test data:
   *     - render element containing the #copyright_notice theme hook
   *     - expected HTML output
   */
  public static function copyRightNoticeProvider(): array {
    $current_year = (int) date('Y');

    $data = [];

    $data['empty'] = [
      [
        '#theme' => 'copyright_notice',
      ],
      '<span>Copyright &copy;, all rights reserved.</span>',
    ];

    $data['name'] = [
      [
        '#theme' => 'copyright_notice',
        '#name' => 'Test',
      ],
      '<span>Copyright &copy; Test, all rights reserved.</span>',
    ];

    $data['base year'] = [
      [
        '#theme' => 'copyright_notice',
        '#name' => 'Test',
        '#year' => 1982,
        '#display_current_year' => FALSE,
        '#attributes' => [],
      ],
      '<span>Copyright &copy; 1982 Test, all rights reserved.</span>',
    ];

    $data['current year'] = [
      [
        '#theme' => 'copyright_notice',
        '#name' => 'Test',
        '#year' => '',
        '#display_current_year' => TRUE,
        '#attributes' => [],
      ],
      '<span>Copyright &copy; ' . $current_year . ' Test, all rights reserved.</span>',
    ];

    $data['base and current year'] = [
      [
        '#theme' => 'copyright_notice',
        '#name' => 'Test',
        '#year' => 1982,
        '#display_current_year' => TRUE,
        '#attributes' => [],
      ],
      '<span>Copyright &copy; 1982 - ' . $current_year . ' Test, all rights reserved.</span>',
    ];

    $data['base and current year equal'] = [
      [
        '#theme' => 'copyright_notice',
        '#name' => 'Test',
        '#year' => $current_year,
        '#display_current_year' => TRUE,
        '#attributes' => [],
      ],
      '<span>Copyright &copy; ' . $current_year . ' Test, all rights reserved.</span>',
    ];

    $data['current greater than base year'] = [
      [
        '#theme' => 'copyright_notice',
        '#name' => 'Test',
        '#year' => $current_year + 1,
        '#display_current_year' => TRUE,
        '#attributes' => [],
      ],
      '<span>Copyright &copy; ' . $current_year . ' - ' . ($current_year + 1) . ' Test, all rights reserved.</span>',
    ];

    return $data;
  }

}
