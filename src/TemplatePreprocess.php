<?php

declare(strict_types=1);

namespace Drupal\copyright_notice;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines a class to define template preprocessors.
 *
 * @internal
 */
class TemplatePreprocess {

  /**
   * Prepares variables for copyright notice templates.
   *
   * Default template: copyright-notice.html.twig.
   *
   * @parm array $variables
   *   An associative array containing:
   *   - name: the name to be printed in the copyright notice, usually this is
   *     the site's name.
   *   - year: the year to be displayed, typically the founding year of the
   *     company/website.
   *   - display_current_year: boolean indicating whether the current year
   *    (interval) should be displayed.
   *   - attributes: associative array of additional attributes for the
   *     surrounding HTML tag.
   */
  public function templatePreprocessCopyrightNotice(array &$variables): void {
    $start_year = $variables['year'] ?? 0;

    // When the current year should also be displayed, make adjustments to the
    // $variables['year'] variable and ensure correct cache invalidation happens
    // at least at the end of the year.
    if ($variables['display_current_year']) {
      // Get the current date and time as an object.
      $now = new DrupalDateTime();

      // Get the current year.
      $current_year = $now->format('Y');

      // Set the year property dependent on the values set for the start and
      // current year.
      if (!empty($start_year)) {
        if ($start_year < $current_year) {
          $variables['year'] = $start_year . ' - ' . $current_year;
        }
        elseif ($start_year > $current_year) {
          $variables['year'] = $current_year . ' - ' . $start_year;
        }
        else {
          $variables['year'] = $current_year;
        }
      }
      else {
        $variables['year'] = $current_year;
      }

      // Get a time representation object for next year.
      $next_year = new DrupalDateTime('1 jan next year');

      // Compute the number of seconds left this year, before the year changes.
      $interval = $next_year->diff($now);
      $seconds_left = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s * $interval->invert;

      // The cache value should be cleared, once a new year starts.
      $variables['#cache']['max-age'] = $seconds_left;
    }
  }

}
