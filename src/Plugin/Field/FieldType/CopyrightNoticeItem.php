<?php

declare(strict_types=1);

namespace Drupal\copyright_notice\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the copyright_notice field type.
 *
 * @FieldType(
 *   id = "copyright_notice",
 *   label = @Translation("Copyright notice"),
 *   description = @Translation("Provides a copyright notice as a field (which for instance can be attached to a node or a block)."),
 *   default_widget = "copyright_notice_widget",
 *   default_formatter = "copyright_notice_formatter",
 * )
 */
class CopyrightNoticeItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t('Starting year'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();

    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $values['value'] = date('Y');

    return $values;
  }

}
