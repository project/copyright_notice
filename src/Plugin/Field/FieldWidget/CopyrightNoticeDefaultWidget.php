<?php

declare(strict_types=1);

namespace Drupal\copyright_notice\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'copyright_notice_widget' widget.
 *
 * @FieldWidget(
 *   id = "copyright_notice_widget",
 *   label = @Translation("Copyright notice"),
 *   field_types = {
 *     "copyright_notice"
 *   }
 * )
 */
class CopyrightNoticeDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value,
      '#size' => 4,
      '#maxlength' => 4,
    ];

    return $element;
  }

}
