<?php

declare(strict_types=1);

namespace Drupal\copyright_notice\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'copyright_notice_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "copyright_notice_formatter",
 *   label = @Translation("Copyright notice"),
 *   field_types = {
 *     "copyright_notice"
 *   }
 * )
 */
class CopyrightNoticeFullFormatter extends FormatterBase {

  /**
   * Constructs a CopyrightNoticeFullFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  final public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, protected ConfigFactoryInterface $configFactory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'display_current_year' => TRUE,
      'display_site_name' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    // Provide a checkbox to indicate whether the current year should be
    // displayed.
    $form['display_current_year'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display current year'),
      '#description' => $this->t('Indicate whether the current year should be displayed.'),
      '#default_value' => $this->getSetting('display_current_year'),
    ];

    // Allow the user to indicate whether the configured site name should be
    // displayed.
    $form['display_site_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display site name'),
      '#description' => $this->t('Indicate whether the site name should be displayed.'),
      '#default_value' => $this->getSetting('display_site_name'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = parent::settingsSummary();

    // Indicate whether the current year is shown.
    $summary[] = $this->getSetting('display_current_year') ?
      $this->t('The current year is displayed in the copyright notice.') :
      $this->t('The current year is not displayed in the copyright notice.');

    // Let the user know whether the configured site name is shown.
    $summary[] = $this->getSetting('display_site_name') ?
      $this->t('The site name is displayed in the copyright notice.') :
      $this->t('The site name is not displayed in the copyright notice.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];

    // Retrieve from the configuration the site's name and the cache tag that
    // should be added.
    $site_name = '';
    if ($this->getSetting('display_site_name')) {
      $system_site_config = $this->configFactory->get('system.site');
      $site_name = $system_site_config->get('name');
      $element['#cache']['tags'] = $system_site_config->getCacheTags();
    }

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'copyright_notice',
        '#year' => $item->value,
        '#display_current_year' => $this->getSetting('display_current_year'),
        '#name' => $site_name,
      ];
    }

    return $element;
  }

}
