# Copyright Notice Field

Provides a field type to store and display copyright notices.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/copyright_notice

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/copyright_notice


## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The copyright notice field can be added to any field-able content type, such as
nodes and blocks. It accepts a year value and a boolean indicating whether to
display the current year. The copyright-notice.html.twig template is used to
render the field.

## Maintainers

Current maintainers:
* Niels Sluijs (Watergate) - https://www.drupal.org/u/watergate

This project has been sponsored by:

* Sicse
  Specialized in creating and managing dynamic web-solutions, incorporating
  state-of-the-art services. Visit https://sicse.dev for more information.
